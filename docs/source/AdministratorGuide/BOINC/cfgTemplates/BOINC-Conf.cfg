DIRAC
{
  Setup = <your production setup>
  Extensions = <your extensions>
  VirtualOrganization = <your VO>
  Configuration
  {
    Name = BOINC-Conf
    MasterServer = dips://<your gateway host>:9135/Configuration/Server
    #Version = 2016-04-13 10:14:21.891327
    Servers = dips://<your gateway host>:9135/Configuration/Server
  }
  Setups
  {
    <your production setup>
    {
      Configuration = boincInstance
      Framework = boincInstance
      RequestManagement = boincInstance
      WorkloadManagement = boincInstance
      ResourceStatus = boincInstance
      Accounting = boincInstance
      DataManagement = boincInstance
    }
  }
}
Resources
{
  FileCatalogs
  {
    FileCatalog
    {
      AccessType = Read-Write
      Status = Active
      Master = True
    }
  }
  # The pilot 2 needs this part. Pilot3 will not anymore.
  Sites
  {
    BOINC
    {
      # name of the site you want to give.
      # It should be the same as in the central CS
      <your BOINC site name>
      {
        CE = BOINC-World-CE.org
        MoUTierLevel = 2
        CEs
        {
          Boinc-World-CE.org
          {
            CEType = BOINC
            Queues
            {
              Boinc.World.Queue
              {
                # The max duration you want your jobs to have
                # This is in minutes !!
                # and yes, it is lowercase !
                maxCPUTime = 120
              }
            }
          }
          Boinc-World-TestCE.org
          {
            CEType = BOINC
            Tag = DoNotTouchMeChris
            Queues
            {
              maxCPUTime = 100000
              Boinc.World.TestQueue
              {
                maxCPUTime = 100000
              }
            }
          }
        }
      }
    }
  }
  StorageElements
  {
    # name of the SE defined in dirac.cfg like BOINC-SE
    # Note that here there is no file protocol !
    <your SE Name>
    {
      StorageBackend = DISET
      ReadAccess = Active
      WriteAccess = Active
      CheckAccess = Active
      RemoveAccess = Active
      AccessProtocol.1
      {
        Host = <gateway host>.cern.ch
        Port = 9148
        PluginName = DIP
        Protocol = dips
        Path = /DataManagement/StorageElement
        Access = remote
      }
    }

    # This must be whatever SandboxSE name you use
    # in your real setup.
    <CertificationSandboxSE>
    {
      BackendType = DISET
      ReadAccess = Active
      WriteAccess = inActive
      CheckAccess = Active
      RemoveAccess = inActive
      AccessProtocol.1
      {
        Host = <your gw host>.cern.ch
        Port = 9199
        PluginName = Proxy
        Protocol = proxy
        Path = /DataManagement/StorageElementProxy
        Access = remote
        SpaceToken =
        WSUrl =
      }
    }

    # You must have a whole bunch of SE defined
    # The names must match what is in the Central CS
    # but the content can be rubish
    # You will need at least
    # * LogSE
    # * your histogram SE


    LogSE
    {
      BackendType = DISET
      ReadAccess = Active
      WriteAccess = Active
      CheckAccess = Active
      RemoveAccess = Active
      AccessProtocol.1
      {
        Host = doesnotmatter.cern.ch
        Port = 9148
        PluginName = DIP
        Protocol = dips
        Path = /DataManagement/StorageElement
        Access = remote
        SpaceToken =
        WSUrl =
      }
    }
    CERN-EOS
    {
      BackendType = Eos
      #@@-phicharp@lhcb_admin - 2017-01-16 10:17:59
      SEType = T0D1
      GFAL2_SRM2
      {
        Host = whatevereos.cern.ch
        Port = 8443
        Protocol = srm
        Path = /eos/base/path
        Access = remote
        SpaceToken = MySpaceToken
        WSUrl = /srm/v2/server?SFN=
      }
    }
    CERN-HIST-EOS
    {
      BaseSE = CERN-EOS
      GFAL2_SRM2
      {
        Path = /eos/lhcb/grid/prod
      }
    }
    CERN-BUFFER
    {
      BaseSE = CERN-EOS
      GFAL2_SRM2
      {
        Path = /eos/lhcb/grid/prod/lhcb/buffer
      }
    }
    CERN-DEBUG
    {
      BaseSE = CERN-EOS
      GFAL2_SRM2
      {
        Path = /eos/lhcb/grid/prod/lhcb/debug
      }
    }
  }
  StorageElementGroups
  {
     # You need to redefine all the SE groups you have in your central CS,
     # but put only here those you have defined above
    Tier1_MC-DST = CERN_MC-DST-EOS
    # the two following lines are needed (both) because we still have somewhere 'BUFFER' in use instead of the new 'Buffer'
    Tier1-Buffer = CERN-BUFFER
    Tier1-BUFFER = CERN-BUFFER
    Tier1-Failover = <your BOINC SE>
  }

}
Operations
{
  <your production setup>
  {

     DataManagement
    {
      # We get the input sandbox through a SE Proxy
      # so we need to add proxy
      AccessProtocols = srm
      AccessProtocols += dips
      AccessProtocols += proxy
    }
    ResourceStatus
    {
      Config
      {
        State = inActive
      }
    }
    Services
    {
      Catalogs
      {
        FileCatalog
        {
          AccessType = Read-Write
          Status = Active
          Master = True
        }
      }
    }
  }
}
Systems
{
  Framework
  {
    boincInstance
    {
      URLs
      {
        SystemAdministrator = dips://<your gw host>:9162/Framework/SystemAdministrator
        ProxyManager = dips://<your gw host>:9133/WorkloadManagement/WMSSecureGW
      }
    }
  }
  Accounting
  {
    boincInstance
    {
      URLs
      {
        # Now the gateway takes care of accounting, so you need to put the GW host and WMSSecureGW service instead of the real accounting services
        DataStore = dips://<your gw host>:9133/WorkloadManagement/WMSSecureGW
        #DataStore = dips://<real data store host>:9147/Accounting/DataStore
        DataStoreHelper = dips://<real data store helper host>:9166/Accounting/DataStoreHelper
      }
    }
  }
  DataManagement
  {
    boincInstance
    {
      URLs
      {
        FileCatalog = dips://<your gw host>:9133/WorkloadManagement/WMSSecureGW
        StorageElement = dips://<your gw host>:9148/DataManagement/StorageElement
        StorageElementProxy = dips://<your gw host>:9199/DataManagement/StorageElementProxy
      }
    }
  }
  WorkloadManagement
  {
    boincInstance
    {
      URLs
      {
        Matcher = dips://<your gw host>:9133/WorkloadManagement/WMSSecureGW
        WMSAdministrator = dips://<your gw host>:9133/WorkloadManagement/WMSSecureGW
        JobStateUpdate = dips://<your gw host>:9133/WorkloadManagement/WMSSecureGW
        JobManager = dips://<your gw host>:9133/WorkloadManagement/WMSSecureGW
        SandboxStore = dips://<your gw host>:9196/WorkloadManagement/SandboxStore
        JobMonitoring = dips://<your gw host>:9133/WorkloadManagement/WMSSecureGW
      }
    }
  }
  RequestManagement
  {
    boincInstance
    {
      URLs
      {
        ReqManager = dips://<your gw host>:9133/WorkloadManagement/WMSSecureGW
        ReqProxyURLs = dips://<your gw host>:9198/RequestManagement/ReqProxy
      }
    }
  }
}
LocalSite
{
  Site = <boinc site name>
}

# Users and groups should be declared here so that
# the proxy info can work at the job level.
Registry
{
  DefaultGroup = boinc_user
  Users
  {
    # Your self generated user certificate
    MrBoinc
    {
      DN = /O=Volunteer Computing/O=CERN/CN=MrBoinc
      CA = /O=Volunteer Computing/CN=Volunteer Computing Signing Certification Authority
    }
  }
  Groups
  {
    boinc_user
    {
      Users = MrBoinc
      Properties = NormalUser
    }
  }
}
