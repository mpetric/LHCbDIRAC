###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# This gitlab ci tests, deploys and build the docker images
# It performs some operations with an account.
# Its credentials are stored as secret variable KRB_PASSWORD and KRB_USERNAME
variables:
    DEPLOY_REPO: "lhcb-core/cvmfs-deploy/deploy-lhcbdirac.git"

# Image used for most of the jobs, unless specified
image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7

stages:
  # sweep immediately after merger
  - sweep
  # If there's a new tag, create its tarball, and upload it.
  - release
   # Run all the tests
  - test
  #  build images
  - images
  # deploy the coverage results
  - coverage
  - coverageDfs
  # If there's a new tag, build the docker images, and also deploy on CVMFS
  - upload_pypi
  - deploy_release
  - update
  # Left over tasks like tagging the devel container and setting the prod symlink
  - finish_up
  - tag


### test stage ###

# Common configuration between pytest, pylint, pycodestyle
.test_setup: &test_setup
  before_script:
    - yum install -y epel-release
    - yum install -y openssl mysql mysql-libs mysql-devel make ncurses-devel libcurl-devel xz-devel jq swig
    - export PYCURL_SSL_LIBRARY=openssl
     # last version supporting python 2
    - pip install 'setuptools<45'
    - pip install --upgrade 'pip<21'
    - pip install diraccfg virtualenv
    # diracTAG is the DIRAC tag on top of which the latest LHCbDIRAC release is built in releases.cfg
    - lhcbdiracTag=$(cd src && python -c "from LHCbDIRAC import version; print(version)")
    - diracTAG=$(diraccfg as-json src/LHCbDIRAC/releases.cfg | jq -r ".Releases.\"${lhcbdiracTag}\".Depends" | cut -d ':' -f2)
    # Clone the latest LHCbDIRAC
    - echo "Using DIRAC tag $diracTAG"
    - git clone https://github.com/DIRACGrid/DIRAC.git
    - cd DIRAC
    - git checkout $diracTAG
    - cd ..
    # Create a virtual environment in which to run the tests
    - virtualenv DIRAC_VENV
    - source DIRAC_VENV/bin/activate
     # last version supporting python 2
    - pip install 'setuptools<45'
    - pip install --upgrade 'pip<21' 'setuptools_scm<6'
    - pip install -r DIRAC/requirements.txt
    # HACK: To be reverted after the next DIRAC release
    - pip install importlib_resources
    - pip install LbEnv LbPlatformUtils uproot backports.lzma lz4 cx_Oracle==7.3 xmltodict

  stage: test
  only:
    refs:
      - branches
    variables:
      - $SWEEP != "true"

.test_setup_py3:
  stage: test
  image: registry.cern.ch/docker.io/condaforge/miniforge3:latest
  before_script:
    - eval "$(python -m conda shell.bash hook)"
    - conda install --yes mamba diraccfg git jq
    - git remote add upstream https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC.git && git fetch upstream
    - lhcbdiracTag=$(git describe --tags --match 'v[0-9][0-9]r*' --abbrev=0)
    - echo "lhcbdiracTag=${lhcbdiracTag}"
    - diracTAG=$(diraccfg as-json src/LHCbDIRAC/releases.cfg | jq -r ".Releases.\"${lhcbdiracTag}\".Depends" | cut -d ':' -f2)
    - echo "diracTAG=${diracTAG}"
    - git clone https://github.com/DIRACGrid/DIRAC.git -b "${diracTAG}" ../DIRAC
    - mamba env create --file ../DIRAC/environment-py3.yml --name test-env
    - conda activate test-env
    - pip install ../DIRAC[server]
    - pip install .[server]

# This runs pylint and fails if there are errors
run_pylint:
  <<: *test_setup
  script:
    - export PYTHONPATH=$(pwd)/DIRAC/src/
    - cd src/LHCbDIRAC
    - 'find . -name "*.py" -and -not -name "NotForJenkins_Test_BKK_DB_OracleBookkeepingDB.py" -exec pylint -E --rcfile=$CI_PROJECT_DIR/DIRAC/.pylintrc --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" --extension-pkg-whitelist=GSI,numpy {} +'

run_pylint_py3:
  extends: .test_setup_py3
  script:
    - pylint -E --rcfile=../DIRAC/.pylintrc src/ tests/

# This runs pytest and generates the coverage report
run_pytest:
  <<: *test_setup
  script:
    - export PYTHONPATH=$(pwd)/DIRAC/src/
    - cd src/LHCbDIRAC
    - DIRAC_DEPRECATED_FAIL=True pytest . --cov=. --cov-report term-missing --cov-report html --junitxml=pytests.xml
  coverage: '/\d+\%\s*$/'
  artifacts:
    paths:
      - src/LHCbDIRAC/htmlcov/

run_pytest_py3:
  extends: .test_setup_py3
  script:
    - DIRAC_DEPRECATED_FAIL=True pytest src/LHCbDIRAC

# This runs pycodestyle
run_pycodestyle:
  <<: *test_setup
  script:
    - lhcbDiracBranch=$(python -c "s = open('src/LHCbDIRAC/releases.cfg').read(); s = s[s.index(' v'):]; s = s.split('\n')[0].strip(); print(s)" | grep -qE '(pre|v0r)'; if [ $? -eq 0 ]; then echo "devel"; else echo "master"; fi)
    - git remote add GL https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC.git
    - git fetch --no-tags GL
    - git branch -avv
    - git diff -U0 GL/${lhcbDiracBranch}...HEAD | pycodestyle --diff

pycodestyle_py3:
  extends: .test_setup_py3
  script:
    - pycodestyle

# Check that the documentation does not generate any error/warning
check_docs:
  extends: .test_setup_py3
  script:
    - pip install ../DIRAC/docs
    - cd docs
    # We need to export the DIRAC env variable so that the makefile knows where to find the documentation tools
    - export DIRACDOCTOOLS=$(dirname $(python -c "import diracdoctools; print(diracdoctools.__file__)"))
    # Remove  numpy from DIRAC_DOC_MOCK_LIST as uproot fails to import without it
    - sed -i "s@'numpy',@@" "${DIRACDOCTOOLS}/__init__.py"
    - SPHINXOPTS=-wsphinxWarnings make htmlall || { echo "Failed to build documentation, check for errors above" ; exit 1; }
    # Check that sphinxWarnings is not empty
    - >
      if [ -s sphinxWarnings ]; then
        echo "********************************************************************************"
        echo "Warnings When Creating Doc:"
        echo "********************************************************************************"
        cat sphinxWarnings
        echo "********************************************************************************"
        exit 1
      fi
  only:
    refs:
      - branches
    variables:
      - $SWEEP != "true"

# This ensures the license text is present
# Based on: https://gitlab.cern.ch/lhcb/LHCb/blob/master/.gitlab-ci.yml#L14-18
check-copyright:
  stage: test
  before_script:
    - git remote add upstream https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC.git
    - git fetch --all
    - curl -o lb-check-copyright "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"
  script:
    - echo $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - python lb-check-copyright upstream/master --exclude '^tests/.*\.xml$' --exclude 'docs/source.conf.py'
  only:
    refs:
      - branches
    variables:
      - $SWEEP != "true"

# Integration tests
integration_tests:
  stage: test
  image: registry.cern.ch/docker.io/library/alpine:edge
  tags:
    - docker-privileged-xl
  services:
    - docker:19.03.1-dind
  variables:
    DOCKER_TLS_CERTDIR: ""
    DOCKER_HOST: tcp://docker:2375/
  rules:
    # Using rules causes "Pipelines for Merge Requests" to be triggered, so disable them explicitly
    # https://docs.gitlab.com/ee/ci/merge_request_pipelines/
    - if: '$LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD == null && $CI_MERGE_REQUEST_ID == null && $SWEEP != "true"'
      allow_failure: true
    - if: '$LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD != null && $CI_MERGE_REQUEST_ID == null && $SWEEP != "true"'
      allow_failure: false
  before_script:
    # Install dependencies
    - apk add docker py3-pip python3 bash git docker-compose jq
    - pip3 install typer pyyaml gitpython diraccfg
    # Clone DIRAC
    - git remote add upstream https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC.git && git fetch upstream
    - lhcbdiracTag=$(git describe --tags --match 'v[0-9][0-9]r*' --abbrev=0)
    - echo "lhcbdiracTag=${lhcbdiracTag}"
    - diracTAG=$(diraccfg as-json src/LHCbDIRAC/releases.cfg | jq -r ".Releases.\"${lhcbdiracTag}\".Depends" | cut -d ':' -f2)
    - diracTAG=rel-$(echo ${diracTAG} | cut -d 'p' -f 1)
    - echo "diracTAG=${diracTAG}"
    - git clone https://github.com/DIRACGrid/DIRAC.git -b "${diracTAG}" ../DIRAC
    - cd ../DIRAC
    # Login to the GitLab container registry for access to Oracle
    - |
      if [[ -z "${LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD:-}" ]]; then
        echo -e '\033[1m\033[31mError LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD variable is not defined!!!\033[0m';
        echo -e 'Due to oracle licensing restrictions CI jobs need a secret to be able able to access the bookkeeping database container.';
        echo -e 'For this test to run you will have to ask another LHCbDIRAC developer to give you access.';
        echo -e "You can then manually copy to a masked variable to your fork's configuration using the instructions at:";
        echo -e ' > https://docs.gitlab.com/ee/ci/variables/#via-the-ui';
        exit 67;
      fi
    - echo "${LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD}" | docker login --username gitlab+deploy-token-794 --password-stdin https://gitlab-registry.cern.ch
  script:
    - python3 ./integration_tests.py create "DIRACOSVER=LHCb:master" "LHCBDIRAC_RELEASE=${lhcbdiracTag}" --extra-module LHCbDIRAC=../LHCbDIRAC

### coverage stage ###

# This job MUST be call pages as per https://docs.gitlab.com/ce/ci/yaml/README.html#pages
pages:
  stage: coverage
  dependencies:
    - run_pytest
  script:
    - TARGET_DIR="public/${CI_COMMIT_REF_NAME}"
    - if [ ! -d "$TARGET_DIR" ]; then mkdir -p $TARGET_DIR; fi
    - echo "Copying into $TARGET_DIR"
    - mv src/LHCbDIRAC/htmlcov/* $TARGET_DIR
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    refs:
      - master@lhcb-dirac/LHCbDIRAC
      - devel@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"

# Deploy the pages generated to DFS
dfsdeploy:
  # Executed during the deploy stage
  stage: coverageDfs
  # # Only when the master branch is pushed
  only:
    refs:
      - master@lhcb-dirac/LHCbDIRAC
      - devel@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  # Custom docker image providing the needed tools to deploy in DFS
  image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
  script:
  # Script that performs the deploy to DFS. Makes use of the variables defined in the project
  # It will not sync the generated content with the folder in DFS (risk of deleting DFS management files)
  # It will just override the contents of DFS or copy new files
  - deploy-dfs
  # do not run any globally defined before_script or after_script for this step
  before_script: []
  after_script: []


# ### release stage ###

# This creates and deploy the tarball
# It is triggered with a tag only
create_tarball:
  image: diracgrid/dirac-distribution:latest
  before_script:
    # Fetch a Kerberos token
    - echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH
    - apt-get install -y curl
  stage: release
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  except:
    - /^v[0-9]+\.[0-9]+.+$/
  variables:
    GIT_STRATEGY: none
  script:
    # Choose from which branch we should get the releases.cfg file
    # We take it from devel if it is a pre release, otherwise master
    - BRANCH=$(echo ${CI_COMMIT_TAG} | grep -qE '^v[1-9][0-9]*r[0-9]+(p[0-9]+)?$'; if [ $? -eq 0 ]; then echo "master"; else echo "devel"; fi)
    # Get the releases.cfg from the appropriate branch
    - curl -O -L https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/raw/$BRANCH/src/LHCbDIRAC/releases.cfg
    # Create the release tarball
    # The last line output by the script is the command to run in order to deploy
    - python3 /dirac-distribution.py -r ${CI_COMMIT_TAG} -l LHCb -C file:///$(pwd)/releases.cfg | tail -n 1  > /tmp/deploy.sh
    - cat /tmp/deploy.sh
    - export USER=${KRB_USERNAME}
    - source /tmp/deploy.sh


# ### deploy_release stage ###

# When we have a git tag, after creating the tarball, create a docker image used by Mesos
# and upload it to the gitlab registry
build_docker_image:
  stage: deploy_release
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  except:
    - /^v[0-9]+\.[0-9]+.+$/
  image:
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - |
      /kaniko/executor \
        --context $CI_PROJECT_DIR/container/lhcbdirac \
        --dockerfile $CI_PROJECT_DIR/container/lhcbdirac/Dockerfile \
        --destination $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG} \
        --build-arg LHCB_DIRAC_VERSION=${CI_COMMIT_TAG}


# Image for propagation of gitlab MRs from master to devel
build_sweeper_image:
  stage: images
  only:
    refs:
      - master@lhcb-dirac/LHCbDIRAC
      - devel@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  image:
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - |
      /kaniko/executor \
        --context $CI_PROJECT_DIR/container/sweeper \
        --dockerfile $CI_PROJECT_DIR/container/sweeper/Dockerfile \
        --destination $CI_REGISTRY_IMAGE:sweeper

# Template job used by deploy_on_cvmfs and set_cvmfs_prod_link
.cvmfs_task:
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  before_script:
    - |
      if [[ "${CI_COMMIT_TAG}" == *"-pre"* || "${CI_COMMIT_TAG}" == *"a"*  ]]; then
        LHCB_DIRAC_SETUP="LHCb-Certification"
      else
        LHCB_DIRAC_SETUP="LHCb-Production"
      fi


# Deploy the release on CVMFS but don't set the prod symlink
deploy_on_cvmfs:
  extends: .cvmfs_task
  stage: deploy_release
  script:
    - ./.ci/trigger_cvmfs_deployment.sh deploy "${LHCB_DIRAC_SETUP}" "${CI_COMMIT_TAG}"


# ### update instance stage ###
# Template job used to get lhcb_admin proxy for production or certification
.getAdminProxy:
  tags:
    - docker
    - cvmfs
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  except:
    - /^v[0-9]+\.[0-9]+.+$/
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  before_script:
    - mkdir -p ~/.globus
    - echo "$BOT_USERCERT_PEM" | tr -d '\r' > ~/.globus/usercert.pem
    - echo "$BOT_USERKEY_PEM" | tr -d '\r' > ~/.globus/userkey.pem
    - chmod 400 ~/.globus/userkey.pem
    - chmod 644  ~/.globus/usercert.pem
    - INSTANCE=$(echo ${CI_COMMIT_TAG} | grep -qE '^v[1-9][0-9]*r[0-9]+(p[0-9]+)?$'; if [ $? -eq 0 ]; then echo "lhcb.cern.ch"; else echo "lhcbdev.cern.ch"; fi)
    - echo "Using LHCbDIRAC installation from /cvmfs/${INSTANCE}/lhcbdirac/"
    - source /cvmfs/${INSTANCE}/lhcbdirac/bashrc
    - echo $BOT_CERT_PASSPHRASE | lhcb-proxy-init -g lhcb_admin -p

update_instance:
  extends: .getAdminProxy
  when: manual
  stage: update
  script:
    - |
      if [ ! -L "/cvmfs/${INSTANCE}/lhcbdirac/${CI_COMMIT_TAG}" ]; then
       echo "New version not found in CVMFS"
       exit 1
      fi
    - |
      if [[ "${INSTANCE}" == "lhcb.cern.ch" ]]; then
        dirac-admin-sysadmin-cli <<< "show hosts" | awk '{print $2}' | grep -E "(\..*){2}" > allHosts.txt
        grep ".cern.ch" allHosts.txt > primaryHosts.txt
        grep -v ".cern.ch" allHosts.txt > secondaryHosts.txt
        dirac-admin-update-instance ${CI_COMMIT_TAG} --retry=2 --hosts=secondaryHosts.txt || echo "Allow failure"
        dirac-admin-update-instance ${CI_COMMIT_TAG} --retry=2 --hosts=primaryHosts.txt
      else
        dirac-admin-update-instance ${CI_COMMIT_TAG} --retry=2
      fi
    - dirac-admin-update-pilot ${CI_COMMIT_TAG}


# ### finish_up stage ###

# If we are triggering on a pre release,
# tag the docker image we just built as 'devel'
# check https://gitlab.cern.ch/ci-tools/docker-image-tools for the image doc
add_devel_tag:
  image: gitlab-registry.cern.ch/ci-tools/docker-image-tools:add-tag
  stage: finish_up
  script:
  - 'Adding tag...'
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
      - ($CI_COMMIT_TAG =~ /.*-pre.*/ && $SWEEP != "true")
  except:
    - /^v[0-9]+\.[0-9]+.+$/
  variables:
    DOCKER_IMAGE_PATH: $CI_REGISTRY_IMAGE
    OLDTAG: ${CI_COMMIT_TAG}
    NEWTAG: devel
    GIT_STRATEGY: none

# Use a manual job to change the version of the prod symlink
# This task can be ran multiple times which means an old pipeline can be restarted to roll back the prod symlink
set_cvmfs_prod_link:
  extends: .cvmfs_task
  except:
    - /^v[0-9]+\.[0-9]+.+$/
  stage: finish_up
  when: manual
  script:
    - ./.ci/trigger_cvmfs_deployment.sh set-prod "${LHCB_DIRAC_SETUP}" "${CI_COMMIT_TAG}"


# Update the Helm configuration
# by setting LHCB_DIRAC_VERSION
# and triggering chaen/diracchart pipeline
deploy_k8:
  stage: finish_up
  allow_failure: true
  variables:
    LHCB_DIRAC_VERSION: ${CI_COMMIT_TAG}
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  except:
    - /^v[0-9]+\.[0-9]+.+$/
  trigger:
    project: chaen/diracchart
    strategy: depend

# Job for starting the tag procedure
make_tag:
  stage: tag
  when: manual
  tags:
    - docker
  variables:
    GIT_STRATEGY: none
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  # Can be run only on master and devel branch of the main repo
  only:
    refs:
      - master@lhcb-dirac/LHCbDIRAC
      - devel@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  except:
    - tags@lhcb-dirac/LHCbDIRAC
  before_script:
    # Remeind the user if he set the variables for versions
    - if [ -z ${DIRAC+x} ]; then echo "variable DIRAC is unset, using the same version as previous release"; else echo "You are targeting DIRAC version ${DIRAC}"; fi
    - if [ -z ${LHCbDIRACOS+x} ]; then echo "variable LHCbDIRACOS is unset, using the same version as previous release"; else echo "You are targeting LHCbDIRACOS version ${LHCbDIRACOS}"; fi
    - if [ -z ${LHCbWebDIRAC+x} ]; then echo "variable LHCbWebDIRAC is unset, using the same version as previous release"; else echo "You are targeting LHCbWebDIRAC version ${LHCbWebDIRAC}"; fi
    - yum install python3 jq git -y
    - curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    - python3 get-pip.py
    - pip3 install -U requests python-dateutil pytz diraccfg
    # Get the tagging tool from the DIRACGrid repo
    - curl -O https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/docs/diracdoctools/scripts/dirac-docs-get-release-notes.py
    - rm -f releases.cfg
    # Get the master version of the releases.cfg from the LHCbDIRAC main repo devel branch
    - curl -O https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/-/raw/devel/src/LHCbDIRAC/releases.cfg
    # init the dirac bot gitlab user
    - eval `ssh-agent -s`
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KEY" | tr -d '\r' > /root/.ssh/id_rsa
    - chmod 700 /root/.ssh/id_rsa
    - ssh-keyscan -p 7999 gitlab.cern.ch >> /root/.ssh/known_hosts
    - git config --global user.email "dirac.bot@cern.ch"
    - git config --global user.name "LHCbDIRAC Bot"
    - git config --global pull.rebase true
  script:
    - echo "GITLABTOKEN = \"$GITLABTOKEN\"" >& GitTokens.py
    # Aggregate the release notes for the current branch
    - python3 dirac-docs-get-release-notes.py --sinceLatestTag -g  -i 3588 --branches ${CI_COMMIT_REF_NAME} | sed '1,2d'>& notes.txt
    # set LATEST_RELEASE variable depending on the branch
    - if [[ "${CI_COMMIT_REF_NAME}" == "master" ]]; then export LATEST_RELEASE=`diraccfg as-json releases.cfg | jq '.Releases' | diraccfg sort-versions | jq -r '.[0]'` ; fi
    - if [[ "${CI_COMMIT_REF_NAME}" == "devel" ]];  then export LATEST_RELEASE=`diraccfg as-json releases.cfg | jq '.Releases' | diraccfg sort-versions --allow-pre-releases | jq -r '.[0]'` ; fi
    - echo "The latest release of LHCbDIRAC is ${LATEST_RELEASE}"
    # remove potential folder coming from artifacts
    - rm -rf LHCbDIRAC
    # checkout the repo
    - git clone ssh://git@gitlab.cern.ch:7999/lhcb-dirac/LHCbDIRAC.git LHCbDIRAC
    - cd LHCbDIRAC
    # checkout the correct branch we can only be on master or devel
    - git checkout ${CI_COMMIT_REF_NAME}
    # get the last releases.cfg
    - mv ../releases.cfg releases.cfg
    # Call the python script that generated all the artifacts for the release
    - python3 src/LHCbDIRAC/tests/Utilities/createReleaseFiles.py
    - mv releases.cfg src/LHCbDIRAC/releases.cfg
    - export SERIES=`cat series.txt`
    - export VERSION=`cat version.txt`
    - export VERSION_PY3=`cat versionPy3.txt`
    # insert new release notes into the changelog
    - cat ../notes.txt CHANGELOG/$SERIES > newNotes.txt
    - mv newNotes.txt CHANGELOG/$SERIES
    # show all the changes we have done
    - cp CHANGELOG/$SERIES ../changes.txt
    - cp src/LHCbDIRAC/releases.cfg ../releases.cfg
    - git diff
    - git add -u
    # commit all the changes to the current branch
    - git commit -av -m "Update versions and changelog for release of ${VERSION}"
    # tag current branch
    - git tag -a ${VERSION} -m ${VERSION}
    - git tag -a ${VERSION_PY3} -m ${VERSION_PY3}
    # push commits and tag to main repo back
    - git push --tags origin ${CI_COMMIT_REF_NAME}
    # If we just tagged master we need to update also the releases.cfg and CHANGELOG in devel
    # Checkout devel and apply the changes
    - git fetch origin
    - git checkout devel
    - git reset --hard origin/devel
    - cp ../changes.txt CHANGELOG/$SERIES
    - cp ../releases.cfg src/LHCbDIRAC/releases.cfg
    # Only push this changes to devel if master was tagged. Don't push if you tagged devel
    - if [[ "${CI_COMMIT_REF_NAME}" == "master" ]]; then git commit -av -m "Add release ${VERSION} to releases.cfg and CHANGELOG"  ; fi
    - if [[ "${CI_COMMIT_REF_NAME}" == "master" ]]; then git push origin devel ; fi
    - cd ..
    # Create release in gitlab
    - echo "GITLABTOKEN = \"$GITLABTOKEN\"" >& GitTokens.py
    - python3 dirac-docs-get-release-notes.py -g -i 3588 --tagName ${VERSION} --releaseNotes notes.txt --deployRelease

MR_SWEEP:
  stage: sweep
  image: gitlab-registry.cern.ch/lhcb-dirac/lhcbdirac:sweeper
  tags:
    - docker
  needs: []
  variables:
    SWEEP_SINCE: "1 month ago"
    SWEEP_UNTIL: "now"
  only:
    refs:
      - master@lhcb-dirac/LHCbDIRAC
  before_script: []
  after_script: []
  script:
    - python /sweep_MR.py -p ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME} -b origin/${CI_COMMIT_REF_NAME} -t ${SWEEP_TOKEN} -s "${SWEEP_SINCE}" -u "${SWEEP_UNTIL}" --repository-root ${PWD}

deploy-to-pypi:
  stage: upload_pypi
  only:
    - tags@lhcb-dirac/LHCbDIRAC
  except:
    - /^v[0-9]+r[0-9]+.+$/
  image: registry.cern.ch/docker.io/library/python:3.9
  before_script:
    - pip install twine wheel setuptools-scm[toml]
  script:
    - python setup.py sdist
    - python setup.py bdist_wheel
    - if [ -z "$TWINE_PASSWORD" ] ; then echo "Set TWINE_PASSWORD in CI variables" ; exit 1 ; fi
    - twine upload -u __token__ dist/*
